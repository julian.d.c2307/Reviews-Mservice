package com.bofugroup.service.review.facade.dto;

import java.sql.Date;

public class DTOinReviews {
	Long id;
	Date commentary_date;
	String commentary;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCommentary_date() {
		return commentary_date;
	}
	public void setCommentary_date(Date commentary_date) {
		this.commentary_date = commentary_date;
	}
	public String getCommentary() {
		return commentary;
	}
	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}
}
