package com.bofugroup.service.review.facade.Imp;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.bofugroup.service.review.business.srv.ReviewsService;
import com.bofugroup.service.review.facade.IReviewsController;
import com.bofugroup.service.review.facade.dto.DTOinReviews;
import com.bofugroup.service.review.facade.dto.DTOoutReviews;
import com.bofugroup.service.review.facade.mapper.IRewviewsMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RequestMapping("/reviews/v00/reviews")
@RestController
@Api(value = "Review Resource", produces = "application/json")
public class ReviewsController implements IReviewsController{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReviewsController.class);
	
	@Autowired
	IRewviewsMapper mapper;
	
	@Autowired
	ReviewsService reviewService;

	@ApiOperation(value = "View a list of avaliable Reviews", response = ArrayList.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully retrieved List"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden "),
			@ApiResponse(code = 404, message = "the resource you were trying to reach is not found")
	})
	@RequestMapping(value="", method=RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<List<DTOoutReviews>> all()
	{
		List<DTOoutReviews> reviews = new ArrayList<>();
		LOGGER.info("init return list of Reviews");
		reviews = mapper.mapOutListReviews(reviewService.all());
		LOGGER.info("return list of Reviews");	
		return new ResponseEntity<List<DTOoutReviews>>(reviews, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Save a Review in BD", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully save in BD"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "Bad Request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden "),
			@ApiResponse(code = 404, message = "the resource you were trying to reach is not found")
	})
	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> add(@RequestBody DTOinReviews dtoInReviews)
	{
		DTOoutReviews response = null;
		response = mapper.mapOutReview(reviewService.save(mapper.mapInReview(dtoInReviews)));
		if (response != null) 
		{
			URI location = ServletUriComponentsBuilder.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(response.getId())
					.toUri();
			return ResponseEntity.created(location).build();
		}
		else
		{
			LOGGER.error("Has errors Review");
			return ResponseEntity.noContent().build();
		}
	}
	
	
	@ApiOperation(value = "Find a Review in BD", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Review Found"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "Bad Request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden "),
			@ApiResponse(code = 404, message = "the resource you were trying to reach is not found")
	})
	@RequestMapping(value = "/{reviewId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<DTOoutReviews> findByReview(@PathVariable("reviewId")Long reviewId)
	{
		LOGGER.error("Has erros Review");
		return new ResponseEntity<DTOoutReviews>(new DTOoutReviews(),HttpStatus.OK);
	}

	
}
