package com.bofugroup.service.review.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bofugroup.service.review.business.dto.Reviews;

@Repository
public interface ReviewsRepository extends CrudRepository<Reviews, Long>
{

}
