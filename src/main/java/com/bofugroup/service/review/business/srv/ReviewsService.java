package com.bofugroup.service.review.business.srv;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.bofugroup.service.review.business.dto.Reviews;
import com.bofugroup.service.review.exception.ExceptionReviewsNotFound;
import com.bofugroup.service.review.repository.ReviewsRepository;



@Component
public class ReviewsService {

	@Autowired
	ReviewsRepository reviewRepository;

	
	public Reviews save(Reviews review) 
	{
		Reviews response = null;
		if(review != null)
			response = reviewRepository.save(review);
		else
			throw new ExceptionReviewsNotFound();
		return response;
	}
	
	public List<Reviews> all()
	{
		return (List<Reviews>) reviewRepository.findAll();
	}
	
	public Optional<Reviews> getId(Long id)
	{
		return reviewRepository.findById(id);
	}
}
